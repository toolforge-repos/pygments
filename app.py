# -*- coding: utf-8 -*-
#
# This file is part of Pygments render API tool.
#
# Copyright (C) 2021 Bryan Davis and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
import flask
import flask_wtf
import pygments
import pygments.formatters
import pygments.lexers
import pygments.util
import werkzeug.middleware.proxy_fix
import wtforms


# Create the Flask application
app = flask.Flask(__name__)

# Add the ProxyFix middleware which reads X-Forwarded-* headers
app.wsgi_app = werkzeug.middleware.proxy_fix.ProxyFix(app.wsgi_app)


class RenderForm(flask_wtf.FlaskForm):
    lang = wtforms.StringField(
        "Language",
        validators=[
            wtforms.validators.InputRequired(),
        ],
    )
    line = wtforms.BooleanField("Enable line numbers", default=False)
    start = wtforms.IntegerField("Starting line number", default=1)
    highlight = wtforms.StringField("Lines to highlight in output")
    inline = wtforms.BooleanField(
        "Output inline element rather than block",
        default=False,
    )
    source = wtforms.TextAreaField(
        "Source code",
        validators=[
            wtforms.validators.InputRequired(),
        ],
    )

    def validate_lang(form, field):
        try:
            pygments.lexers.get_lexer_by_name(field.data)
        except pygments.util.ClassNotFound:
            raise wtforms.validators.ValidationError("Unknown language")


@app.route("/")
def index():
    """Application landing page."""
    form = RenderForm(meta={"csrf": False})
    return flask.render_template("index.html", form=form)


@app.route("/render/", methods=["POST"])
def render():
    form = RenderForm(meta={"csrf": False})
    if form.validate():
        source = form.source.data
        lexer = pygments.lexers.get_lexer_by_name(form.lang.data)
        opts = {
            "cssclass": "mw-highlight",
            "encoding": "utf-8",
            "linenostart": form.start.data,
        }
        if form.line.data:
            opts["linenos"] = "inline"
        if form.lang.data == "php" and "<?php" not in source:
            opts["startinline"] = True
        if form.highlight.data:
            lines = []
            for v in form.highlight.data.strip().split(","):
                if v.isnumeric():
                    lines.append(v)
                elif "-" in v:
                    # FIXME: validate ranges
                    start, end = v.split("-")
                    lines.extend(
                        str(x) for x in range(int(start), int(end) + 1)
                    )
                # FIXME: max highlight limit?
            opts["hl_lines"] = " ".join(lines)
        if form.inline.data:
            opts["nowrap"] = True

        formatter = pygments.formatters.get_formatter_by_name("html", **opts)
        return pygments.highlight(source, lexer, formatter)
